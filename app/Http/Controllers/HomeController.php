<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

 
    // public function index()
    // {
    //     return view('home');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dash()
    {
        return view('partials.dashboard');
    }

    public function salesdash()
    {
        return view('partials.sales-dashboard');
    }

    public function marketingdash()
    {
        return view('partials.marketing-dashboard');
    }

    public function socialdash()
    {
        return view('partials.social-dashboard');
    }

    public function anadash()
    {
        return view('partials.analytical-dashboard');
    }
}
