<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserinfoRequest;
use App\Http\Requests\UpdateUserinfoRequest;
use App\Repositories\UserinfoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserinfoController extends AppBaseController
{
    /** @var  UserinfoRepository */
    private $userinfoRepository;

    public function __construct(UserinfoRepository $userinfoRepo)
    {
        $this->userinfoRepository = $userinfoRepo;
    }

    /**
     * Display a listing of the Userinfo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userinfoRepository->pushCriteria(new RequestCriteria($request));
        $userinfos = $this->userinfoRepository->all();

        return view('userinfos.index')
            ->with('userinfos', $userinfos);
    }

    /**
     * Show the form for creating a new Userinfo.
     *
     * @return Response
     */
    public function create()
    {
        return view('userinfos.create');
    }

    /**
     * Store a newly created Userinfo in storage.
     *
     * @param CreateUserinfoRequest $request
     *
     * @return Response
     */
    public function store(CreateUserinfoRequest $request)
    {
        $input = $request->all();

        $userinfo = $this->userinfoRepository->create($input);

        Flash::success('Userinfo saved successfully.');

        return redirect(route('userinfos.index'));
    }

    /**
     * Display the specified Userinfo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userinfo = $this->userinfoRepository->findWithoutFail($id);

        if (empty($userinfo)) {
            Flash::error('Userinfo not found');

            return redirect(route('userinfos.index'));
        }

        return view('userinfos.show')->with('userinfo', $userinfo);
    }

    /**
     * Show the form for editing the specified Userinfo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userinfo = $this->userinfoRepository->findWithoutFail($id);

        if (empty($userinfo)) {
            Flash::error('Userinfo not found');

            return redirect(route('userinfos.index'));
        }

        return view('userinfos.edit')->with('userinfo', $userinfo);
    }

    /**
     * Update the specified Userinfo in storage.
     *
     * @param  int              $id
     * @param UpdateUserinfoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserinfoRequest $request)
    {
        $userinfo = $this->userinfoRepository->findWithoutFail($id);

        if (empty($userinfo)) {
            Flash::error('Userinfo not found');

            return redirect(route('userinfos.index'));
        }

        $userinfo = $this->userinfoRepository->update($request->all(), $id);

        Flash::success('Userinfo updated successfully.');

        return redirect(route('userinfos.index'));
    }

    /**
     * Remove the specified Userinfo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userinfo = $this->userinfoRepository->findWithoutFail($id);

        if (empty($userinfo)) {
            Flash::error('Userinfo not found');

            return redirect(route('userinfos.index'));
        }

        $this->userinfoRepository->delete($id);

        Flash::success('Userinfo deleted successfully.');

        return redirect(route('userinfos.index'));
    }
}
