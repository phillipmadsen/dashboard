<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Userinfo",
 *      required={""},
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="photo",
 *          description="photo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="f_name",
 *          description="f_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="l_name",
 *          description="l_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="about_me",
 *          description="about_me",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="website",
 *          description="website",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="company",
 *          description="company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gender",
 *          description="gender",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mobile",
 *          description="mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="work",
 *          description="work",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="other",
 *          description="other",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_published",
 *          description="is_published",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_active",
 *          description="is_active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="dob",
 *          description="dob",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="skypeid",
 *          description="skypeid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="githubid",
 *          description="githubid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="twitter_username",
 *          description="twitter_username",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="instagram_username",
 *          description="instagram_username",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="facebook_username",
 *          description="facebook_username",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="facebook_url",
 *          description="facebook_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="linked_in_url",
 *          description="linked_in_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="google_plus_url",
 *          description="google_plus_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      )
 * )
 */
class Userinfo extends Model
{
    

    public $table = 'userinfo';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    public $fillable = [
        'photo',
        'f_name',
        'l_name',
        'uuid',
        'about_me',
        'website',
        'company',
        'gender',
        'phone',
        'mobile',
        'work',
        'other',
        'is_published',
        'is_active',
        'dob',
        'skypeid',
        'githubid',
        'twitter_username',
        'instagram_username',
        'facebook_username',
        'facebook_url',
        'linked_in_url',
        'google_plus_url',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'photo' => 'string',
        'f_name' => 'string',
        'l_name' => 'string',
        'uuid' => 'string',
        'about_me' => 'string',
        'website' => 'string',
        'company' => 'string',
        'gender' => 'string',
        'phone' => 'string',
        'mobile' => 'string',
        'work' => 'string',
        'other' => 'string',
        'is_published' => 'boolean',
        'is_active' => 'boolean',
        'dob' => 'date',
        'skypeid' => 'string',
        'githubid' => 'string',
        'twitter_username' => 'string',
        'instagram_username' => 'string',
        'facebook_username' => 'string',
        'facebook_url' => 'string',
        'linked_in_url' => 'string',
        'google_plus_url' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function setUuid($uuid) {
        return Uuid::generate(3, $this->f_name . $this->l_name, Uuid::NS_DNS);
    }

    public function get_display_name()
    {
        return $this->f_name .' '.$this->l_name;
    }


    public function getDobAttribute($value) {
        if ($value == '0000-00-00 00:00:00') {
            return NULL;
        } else if ($value) {
            return Carbon::parse($value)->format('m/d/Y');
        } else {

            return $value;
        }
    }


}
