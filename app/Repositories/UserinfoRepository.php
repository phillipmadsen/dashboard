<?php

namespace App\Repositories;

use App\Models\Userinfo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserinfoRepository
 * @package App\Repositories
 * @version November 27, 2017, 11:08 pm UTC
 *
 * @method Userinfo findWithoutFail($id, $columns = ['*'])
 * @method Userinfo find($id, $columns = ['*'])
 * @method Userinfo first($columns = ['*'])
*/
class UserinfoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'photo',
        'f_name',
        'l_name',
        'uuid',
        'about_me',
        'website',
        'company',
        'gender',
        'phone',
        'mobile',
        'work',
        'other',
        'is_published',
        'is_active',
        'dob',
        'skypeid',
        'githubid',
        'twitter_username',
        'instagram_username',
        'facebook_username',
        'facebook_url',
        'linked_in_url',
        'google_plus_url',
        'slug'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Userinfo::class;
    }
}
