<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span><strong>Users</strong></span></a>
</li>

<li class="{{ Request::is('userinfos*') ? 'active' : '' }}">
    <a href="{!! route('userinfos.index') !!}"><i class="fa fa-edit"></i><span><strong>UserInfos</strong></span></a>
</li>
 