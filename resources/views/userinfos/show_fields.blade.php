<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $userinfo->user_id !!}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{!! $userinfo->photo !!}</p>
</div>

<!-- F Name Field -->
<div class="form-group">
    {!! Form::label('f_name', 'F Name:') !!}
    <p>{!! $userinfo->f_name !!}</p>
</div>

<!-- L Name Field -->
<div class="form-group">
    {!! Form::label('l_name', 'L Name:') !!}
    <p>{!! $userinfo->l_name !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $userinfo->uuid !!}</p>
</div>

<!-- About Me Field -->
<div class="form-group">
    {!! Form::label('about_me', 'About Me:') !!}
    <p>{!! $userinfo->about_me !!}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{!! $userinfo->website !!}</p>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:') !!}
    <p>{!! $userinfo->company !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $userinfo->gender !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $userinfo->phone !!}</p>
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'Mobile:') !!}
    <p>{!! $userinfo->mobile !!}</p>
</div>

<!-- Work Field -->
<div class="form-group">
    {!! Form::label('work', 'Work:') !!}
    <p>{!! $userinfo->work !!}</p>
</div>

<!-- Other Field -->
<div class="form-group">
    {!! Form::label('other', 'Other:') !!}
    <p>{!! $userinfo->other !!}</p>
</div>

<!-- Is Published Field -->
<div class="form-group">
    {!! Form::label('is_published', 'Is Published:') !!}
    <p>{!! $userinfo->is_published !!}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{!! $userinfo->is_active !!}</p>
</div>

<!-- Dob Field -->
<div class="form-group">
    {!! Form::label('dob', 'Dob:') !!}
    <p>{!! $userinfo->dob !!}</p>
</div>

<!-- Skypeid Field -->
<div class="form-group">
    {!! Form::label('skypeid', 'Skypeid:') !!}
    <p>{!! $userinfo->skypeid !!}</p>
</div>

<!-- Githubid Field -->
<div class="form-group">
    {!! Form::label('githubid', 'Githubid:') !!}
    <p>{!! $userinfo->githubid !!}</p>
</div>

<!-- Twitter Username Field -->
<div class="form-group">
    {!! Form::label('twitter_username', 'Twitter Username:') !!}
    <p>{!! $userinfo->twitter_username !!}</p>
</div>

<!-- Instagram Username Field -->
<div class="form-group">
    {!! Form::label('instagram_username', 'Instagram Username:') !!}
    <p>{!! $userinfo->instagram_username !!}</p>
</div>

<!-- Facebook Username Field -->
<div class="form-group">
    {!! Form::label('facebook_username', 'Facebook Username:') !!}
    <p>{!! $userinfo->facebook_username !!}</p>
</div>

<!-- Facebook Url Field -->
<div class="form-group">
    {!! Form::label('facebook_url', 'Facebook Url:') !!}
    <p>{!! $userinfo->facebook_url !!}</p>
</div>

<!-- Linked In Url Field -->
<div class="form-group">
    {!! Form::label('linked_in_url', 'Linked In Url:') !!}
    <p>{!! $userinfo->linked_in_url !!}</p>
</div>

<!-- Google Plus Url Field -->
<div class="form-group">
    {!! Form::label('google_plus_url', 'Google Plus Url:') !!}
    <p>{!! $userinfo->google_plus_url !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $userinfo->slug !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $userinfo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $userinfo->updated_at !!}</p>
</div>

