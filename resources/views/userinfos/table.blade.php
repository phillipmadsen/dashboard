<table class="table table-responsive" id="userinfos-table">
    <thead>
        <tr>
            <th>User ID</th>
            {{-- <th>Photo</th> --}}
            <th>F Name</th>
            <th>L Name</th>
            <th>Uuid</th>
     {{--        <th>About Me</th>
            <th>Website</th>
            <th>Company</th>
            <th>Gender</th> --}}
            <th>Phone</th>
        {{--     <th>Mobile</th>
            <th>Work</th>
            <th>Other</th> --}}
            <th>Is Published</th>
            <th>Is Active</th>
         {{--    <th>Dob</th>
            <th>Skypeid</th>
            <th>Githubid</th>
            <th>Twitter Username</th>
            <th>Instagram Username</th>
            <th>Facebook Username</th>
            <th>Facebook Url</th>
            <th>Linked In Url</th>
            <th>Google Plus Url</th>
            <th>Slug</th> --}}
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($userinfos as $userinfo)
        <tr>
            <td>{!! $userinfo->user_id !!}</td>
            {{-- <td>{!! $userinfo->photo !!}</td> --}}
            <td>{!! $userinfo->f_name !!}</td>
            <td>{!! $userinfo->l_name !!}</td>
{{--             <td>{!! $userinfo->uuid !!}</td>
            <td>{!! $userinfo->about_me !!}</td>
            <td>{!! $userinfo->website !!}</td>
            <td>{!! $userinfo->company !!}</td>
            <td>{!! $userinfo->gender !!}</td> --}}
            <td>{!! $userinfo->phone !!}</td>
           {{--  <td>{!! $userinfo->mobile !!}</td>
            <td>{!! $userinfo->work !!}</td>
            <td>{!! $userinfo->other !!}</td> --}}
            <td>{!! $userinfo->is_published !!}</td>
            <td>{!! $userinfo->is_active !!}</td>
          {{--   <td>{!! $userinfo->dob !!}</td>
            <td>{!! $userinfo->skypeid !!}</td>
            <td>{!! $userinfo->githubid !!}</td>
            <td>{!! $userinfo->twitter_username !!}</td>
            <td>{!! $userinfo->instagram_username !!}</td>
            <td>{!! $userinfo->facebook_username !!}</td>
            <td>{!! $userinfo->facebook_url !!}</td>
            <td>{!! $userinfo->linked_in_url !!}</td>
            <td>{!! $userinfo->google_plus_url !!}</td> --}}
            {{-- <td>{!! $userinfo->slug !!}</td> --}}
            <td>
                {!! Form::open(['route' => ['userinfos.destroy', $userinfo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('userinfos.show', [$userinfo->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye fa-2x"></i></a>
                    <a href="{!! route('userinfos.edit', [$userinfo->id]) !!}" class='btn btn-warning btn-xs'><i class="fa fa-pencil-square-o fa-2x"></i></a>
                    {!! Form::button('<i class="fa fa-trash fa-2x"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>